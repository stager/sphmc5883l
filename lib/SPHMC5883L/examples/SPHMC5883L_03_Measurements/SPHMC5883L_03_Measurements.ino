#include <Wire.h>

#include <SPHMC5883L.h>

const byte i2cAddr = 0x1E;

SPHMC5883L compass(i2cAddr);

void setup()
 {
  Serial.begin(9600);
  Wire.begin();

  /* Provide calibration information which was                     */
  /* gathered during compass calibration (see "Calibration" demo)  */
  /* You need to calibrate each chip, do not use predefined values */

  compass.setCalibration(27.13f,-187.9f,54.27f);

  /* Configure compase to average 8 measurements, read data 15 times a */
  /* second, use GAIN5                                                 */
  compass.configure(SPHMC5883L::SAMPLES_8,
                    SPHMC5883L::DATA_RATE_15,
                    SPHMC5883L::GAIN5,
                    SPHMC5883L::MODE_CONTINUOUS);

  /* First measurements will be available after ~2 periods */
  /* 1 / 15 * 2 ~~ 0.133                                   */
  delay(134);
 }

float getBearing(float x, float y, float z)
 {
  float bearing;

  bearing = atan2f(y,x) * 180.0f / 3.1415927f;

  if (bearing < 0.0f)
   {
    bearing += 360.0f;
   }

  return bearing;
 }

void loop()
 {
  float x,y,z;

  compass.readXYZ(&x,&y,&z);

  Serial.print("X(mG): ");
  Serial.print(x);
  Serial.print(" Y(mG): ");
  Serial.print(y);
  Serial.print(" Z(mG): ");
  Serial.print(z);

  Serial.print(" bearing(degrees): ");
  Serial.println(getBearing(x,y,z));

  delay(500);
 }

