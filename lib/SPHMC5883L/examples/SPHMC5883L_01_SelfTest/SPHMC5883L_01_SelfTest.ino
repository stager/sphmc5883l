#include <Wire.h>

#include <SPHMC5883L.h>

const byte i2cAddr = 0x1E;

SPHMC5883L compass(i2cAddr);

void setup()
 {
  Serial.begin(9600);
  Wire.begin();

  Serial.println("HMC5883L self test started...");

  if (compass.selfTest())
   {
    Serial.println("Passed");
   }
  else
   {
    Serial.println("FAILED");
   }
 }

void loop()
 {
 }

