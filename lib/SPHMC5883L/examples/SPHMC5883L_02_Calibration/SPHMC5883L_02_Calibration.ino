#include <Wire.h>

#include <SPHMC5883L.h>

const byte i2cAddr = 0x1E;
const byte calibrationTime = 30;

SPHMC5883L compass(i2cAddr);

void setup()
 {
  Serial.begin(9600);
  Wire.begin();

  Serial.println("HMC5883L calibration started,");
  Serial.print("Please rotate compass around all axes for ~30 seconds...");

  compass.calibrate(calibrationTime);

  float offsetX,offsetY,offsetZ;

  compass.getCalibration(&offsetX,&offsetY,&offsetZ);

  Serial.println("Done!");
  Serial.println();

  Serial.println("Write down calibration results and pass them as");
  Serial.println("the arguments to setCalibration call before using");
  Serial.println("getXYZ method of HMC5883L.");

  Serial.println();

  Serial.print("Offset X: ");
  Serial.println(offsetX);
  Serial.print("Offset Y: ");
  Serial.println(offsetY);
  Serial.print("Offset Z: ");
  Serial.println(offsetZ);
 }

void loop()
 {
 }

